from django.shortcuts import render

# Create your views here.
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from .serializer import AccountSerializer
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from datetime import datetime
import time
from django.db.models import F
from .models import *

class HelloView(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
    	content = {'message': 'Hello, World!'}
    	return Response(content)

class InitView(APIView):
	def post(self, request):
		serialized = AccountSerializer(data=request.data)
		serialized.is_valid(raise_exception=True)
		user, user_created = User.objects.get_or_create(username=serialized.validated_data['customer_xid'],password='default')
		account = Account.objects.get_or_create(customer_xid=serialized.validated_data['customer_xid'])
		token,created=Token.objects.get_or_create(user=user)
		return Response({'data':{'token':token.key},'status':'success'})

class EnableView(APIView):
	permission_classes = (IsAuthenticated,)

	def post(self,request):
		token = request.auth
		user = Token.objects.filter(key=token).values('user__username')
		if user:
			if Wallet.objects.filter(account__customer_xid=user[0]['user__username'], status='enabled').exists():
				if request.data.get('is_disabled',False):
					wallet = Wallet.objects.get(account__customer_xid=user[0]['user__username'], status='enabled')
					wallet.status= 'disabled'
					wallet.disabled_at = datetime.now()
					wallet.save()
					result = {
						'status': 'success',
						'data': {
							'wallet': {
								'id': wallet.account.customer_xid,
								'owned_by': request.user.username,
								'status': wallet.status,
								'disabled_at': wallet.disabled_at,
								'balance': wallet.balance
							}
						}
					}
					return Response(result)
				return Response({'error':'Wallet already Enabled'},status=404)
			else:
				account = Account.objects.get(customer_xid=user[0]['user__username'])
				if Wallet.objects.filter(account__customer_xid=user[0]['user__username']).exists():
					Wallet.objects.filter(account__customer_xid=user[0]['user__username']).update({'status':'enabled','enabled_at':datetime.now()})
					wallet = Wallet.objects.get(account=account,status='enabled')
				else:
					wallet = Wallet.objects.create(account=account,status='enabled',enabled_at=datetime.now())
				result = {
					'status': 'success',
					'data': {
						'wallet': {
							'id': wallet.account.customer_xid,
							'owned_by': request.user.username,
							'status': wallet.status,
							'enabled_at': wallet.enabled_at,
							'balance': wallet.balance
						}
					}
				}
				time.sleep(5)
				return Response(result)

	def get(self,request):
		token = request.auth
		user = Token.objects.filter(key=token).values('user__username')
		if user:
			if Wallet.objects.filter(account__customer_xid=user[0]['user__username'], status='enabled').exists():
				wallet = Wallet.objects.filter(account__customer_xid=user[0]['user__username'], status='enabled').values('account__customer_xid','status','enabled_at','balance')[0]
				result = {
					'status': 'success',
					'data': {
						'wallet': {
							'id': wallet['account__customer_xid'],
							'owned_by': request.user.username,
							'status': wallet['status'],
							'enabled_at': wallet['enabled_at'],
							'balance': wallet['balance']
						}
					}
				}
				return Response(result)
			else:
				return Response({'error':'Wallet needs to be enabled'},status=404)

class DepositView(APIView):
	permission_classes = (IsAuthenticated,)

	def post(self, request):
		token = request.auth
		user = Token.objects.filter(key=token).values('user__username')
		if user:
			if Wallet.objects.filter(account__customer_xid=user[0]['user__username'], status='enabled').exists():
				wallet = Wallet.objects.get(account__customer_xid=user[0]['user__username'], status='enabled')
				amount = request.data.get('amount',0)
				reference = request.data.get('reference_id',None)
				if amount and reference:
					wallet.balance = F('balance') + amount
					wallet.reference_id = reference
					wallet.deposited_by = request.user
					wallet.deposited_at = datetime.now()
					wallet.reference_id = reference
					wallet.save()
					result = {
						'status': 'success',
						'data':{
							'deposit': {
								'id': wallet.account.customer_xid,
								'deposited_by': wallet.deposited_by.username,
								'status': wallet.status,
								'deposited_at': wallet.deposited_at,
								'amount': amount,
								'reference_id': wallet.reference_id
							}
						}
					}
					return Response(result)
				else:
					return Response({'error':'Amount and Reference ID must be there'},status=404)

			else:
				return Response({'error':'Wallet needs to be enabled'},status=404)

class WithdrawView(APIView):
	permission_classes = (IsAuthenticated,)

	def post(self, request):
		token = request.auth
		user = Token.objects.filter(key=token).values('user__username')
		reference = request.data.get('reference_id',None)
		if user and reference:
			if Wallet.objects.filter(account__customer_xid=user[0]['user__username'], status='enabled', reference_id=reference).exists():
				wallet = Wallet.objects.get(account__customer_xid=user[0]['user__username'], status='enabled', reference_id=reference)
				amount = amount = request.data.get('amount',0)
				if amount and wallet.balance > amount:
					wallet.balance = F('balance') - amount
					wallet.withdrawn_by = request.user
					wallet.withdrawn_at = datetime.now()
					wallet.save()
					result = {
						'status': 'success',
						'data': {
							'withdrawl': {
								'id': wallet.account.customer_xid,
								'withdrawn_by': wallet.withdrawn_by.username,
								'status': wallet.status,
								'withdrawn_at': wallet.withdrawn_at,
								'amount': amount,
								'reference_id': wallet.reference_id
							}
						}
					}
					return Response(result)
				else:
					return Response({'error':'Can not withdraw more than the wallet balance'},status=404)
			else:
				return Response({'error':'Wallet needs to be enabled'},status=404)
		else:
			return Response({'error':'Reference ID Needed'},status=404)