from django.urls import path
from .views import *

urlpatterns = [
    path('', HelloView.as_view()),
    path('v1/init',InitView.as_view()),
    path('v1/wallet',EnableView.as_view()),
    path('v1/wallet/deposits',DepositView.as_view()),
    path('v1/wallet/withdrawals',WithdrawView.as_view()),    
]
