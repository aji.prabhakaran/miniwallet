from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Account(models.Model):
	customer_xid = models.CharField(max_length=64)

	def __str__(self):
		return self.customer_xid

class Wallet(models.Model):
	account = models.ForeignKey(Account, on_delete=models.CASCADE, related_name='account')
	status = models.CharField(max_length=64,default='disabled')
	enabled_at = models.DateTimeField()
	disabled_at = models.DateTimeField(null=True, blank=True)
	balance = models.IntegerField(default=0)
	deposited_at = models.DateTimeField(null=True, blank=True)
	deposited_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name='deposited_by', null=True, blank=True)
	withdrawn_at = models.DateTimeField(null=True, blank=True)
	withdrawn_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name='withdrawn_by', null=True, blank=True)
	reference_id = models.CharField(max_length=64,unique=True)

	def __str__(self):
		return self.status